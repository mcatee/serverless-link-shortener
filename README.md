This is a link shortener built with Node.js and the Serverless Framework


# This page (for non-developers)

This page is a view into the source code for this service.
Serverless code is handy because it really let's _anyone_ deploy as little or as much code as they'd like to the cloud - which drops the barrier to administering a service like this one! 

If you feel comfortable (and know how to open the terminal app!), please continue to the next section.
If you don't, it may be good to delegate this to somebody more technically experienced - they'll need:

* a link to this page that you're reading
* credentials for your AWS account (or the respective provider you're intending to deploy to) 
* an idea of the configuration you have in mind, for example:
 * The **Domain** you'd like the main site to be pointed at by
 * The **Subdomain** that should be used for links ("https://**links**.shor.tw/....")
 * _and that's it for now!_


# Developers - project structure and deployment
The project makes use of two serverless distributions which are stored in two top-level directories: **infrastructure**, and **functions**.

## Running with serverless-offline & domain-manager