
# Next
1. Retrieve and display records from tables defined in infrastructure YAML


# Future
1. Add an `EventBridge` function - when a new entry is added to the linkMapping table, the index is regenerated (once settings menu is available, make this togglable)
