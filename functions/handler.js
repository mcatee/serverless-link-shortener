'use strict';

var AWS = require('aws-sdk');
var docClient = new AWS.DynamoDB.DocumentClient();  //I know, redundant

module.exports.hello = async (event) => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Go Serverless v1.0! Your function executed successfully!',
        input: event,
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

function isNullOrUndefined(item) {
  return (item == undefined) || (item == null);
}


function getLinkMapRecord(primaryRef, categoryRef) {
  if(isNullOrUndefined(primaryRef) && isNullOrUndefined(categoryRef)) {
    throw {
      statusCode: 400,
      body: JSON.stringify({
        responseType: "error",
        message: "No references in URL"
      })
    }
  } //END REF PATH PARAM EXISTENCE CHECK

  var keyParams = {
  }

  if(isNullOrUndefined(primaryRef)) {
    keyParams['pageRef'] = categoryRef;
  } else {
    keyParams['pageRef'] = categoryRef;
    keyParams['categoryRef'] = primaryRef;
  }
  //TODO add DB logic here
  return docClient.get({
        TableName: process.env.LinkTableName,
        Key: keyParams
  }).promise()
    .then(data => {
      console.log("DATA");
      console.log(data);
      console.log("--DATA\n\n");
      if(data.Item == undefined) {
          console.log("not defined");
          return {
            "result": null,
          }
          // throw {
          //     statusCode: 404,
          //     body: JSON.stringify({
          //         "error": "LinkMappingNotFound",
          //         "reason": `Could not read '${JSON.stringify(keyParams)}' from database. This data may have been removed, may not have existed, or you may not have sufficient permissions to see the data.`
          //     })
          // }
      }
      console.log("Found:");
      console.log(data);
      console.log(data.Item);
      return {"result": data.Item};
    });

//  return keyParams;
}


module.exports.resolveRedirect = async(event) => {
  return getLinkMapRecord(event.pathParameters.primaryRef, event.pathParameters.secondaryRef).then(linkMapping => {
    console.log("Back in main body...");
    console.log(linkMapping);
    if(linkMapping["result"] == null){
      console.log("Returning 404 page")
      return {
        statusCode: 200,
        body: `<html><head></head><body>Mapping not found, <a href="#"> return to the index</a></body><html>`,
        headers: {
          "Content-Type": "text/html",
        },
      };
    } else {
      console.log("Returning link page");
  
      var url = linkMapping['result']["url"];
      return {
        statusCode: 200,
        body: `<html><head></head><body>Mapping found, redirecting you to <a href="${url}"> the destination page</a></body><html>`,
        headers: {
          "Content-Type": "text/html",
        }
      };
    }
  })
  

  

  
}

